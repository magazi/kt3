import java.util.LinkedList;
import java.util.NoSuchElementException;

public class LongStack {

   public static void main (String[] argum) {

   }

   private LinkedList<Long> longStack = new LinkedList<>();

   LongStack() {
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clone = new LongStack();
      for (int i = 0; i < this.longStack.size(); i++) {
         clone.longStack.addLast(this.longStack.get(i));
      }
      return clone;
   }

   public boolean stEmpty() {
      return this.longStack.size() <= 0;
   }

   public void push (long a) {
      this.longStack.addFirst(a);
   }

   public long pop() {
      if (stEmpty()) {
         throw new NoSuchElementException("There are no more numbers left to take.");
      }
      return this.longStack.pop();
   }

   public void op (String s) throws NoSuchElementException {
      try {
         long lastElement = this.longStack.pop();
         long lastBeforeLastElement = this.longStack.pop();


         switch (s) {
            case "+":
               this.longStack.push(lastBeforeLastElement + lastElement);
               break;
            case "-":
               this.longStack.push(lastBeforeLastElement - lastElement);
               break;
            case "*":
               this.longStack.push(lastBeforeLastElement * lastElement);
               break;
            case "/":
               this.longStack.push(lastBeforeLastElement / lastElement);
               break;
            case "SWAP":

               this.longStack.push(lastElement);
               this.longStack.push(lastBeforeLastElement);
               break;
            case "ROT":
               long lastLastElement = this.longStack.pop();
               this.longStack.push(lastBeforeLastElement);
               this.longStack.push(lastElement);
               this.longStack.push(lastLastElement);
               break;
            default:
               System.out.printf("Operator '%s' is not an acceptable arithmetical operator.\n" +
                       "Only '+', '-', '*' or '/' are allowed.\n", s);
         }
      } catch (NoSuchElementException e) {
         throw new NoSuchElementException("There are not enough elements to do the operation: " + s);
      }
   }

   public long tos() {
      if (stEmpty()) {
         throw new NoSuchElementException("There are no numbers to get");
      }
      return this.longStack.getFirst();
   }


   @Override
   public boolean equals (Object o) {
      // Return false if stack sizes are different.
      if (((LongStack) o).longStack.size() != this.longStack.size()) {
         return false;
      }

      // Start comparing both LongStack one by one. Return false if mismatch is found.
      for (int i = 0; i < this.longStack.size(); i++) {
         if (((LongStack) o).longStack.get(i) != this.longStack.get(i)) {
            return false;
         }
      }
      // Return true if no mismatch is found.
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "Empty";
      }
      StringBuffer stringBuffer = new StringBuffer();
      for (int i = this.longStack.size() - 1; i >= 0; i--)
         stringBuffer.append(this.longStack.get(i) + " ");
      return stringBuffer.toString();
   }



   public static long interpret (String pol) {
      // Remove all unnecessary formatting.
      String strippedPol = pol
              .replaceAll("\t", "")
              .replaceAll("\n", "")
              .replaceAll("[ ](?=[ ])","")
              .trim();

      // Throw exception if no arguments are left after removing formatting.
      if (strippedPol.isEmpty()) {
         throw new IllegalArgumentException("Notation does not contain elements.");
      }

      // Split the string into array using spaces between as separators.
      String[] polArray = strippedPol.split(" ");


      // Use stack to do the calculations.
      LongStack resultStack = new LongStack();
      for (int i = 0; i < polArray.length; i++) {
         if (polArray[i].matches("[+*/\\-]") || polArray[i].equals("SWAP") ||
                 polArray[i].equals("ROT")) {
            try {
               resultStack.op(polArray[i]);
            }
            catch (NoSuchElementException e) {
               throw new IllegalArgumentException("The given notation " + strippedPol +
                       " contains too few numbers for a operator to complete the calculation.");
            }
         }
         else {
            resultStack.push(Long.parseLong(polArray[i]));
         }
      }

      // Return only if the size of the stack is 1.
      // If not 1, then the calculations were unfinished, numbers were left and exception is thrown.
      if (resultStack.longStack.size() == 1) {
         return resultStack.pop();
      } else {
         throw new IllegalArgumentException("The given notation " + strippedPol +
                 " contains too few operators for given numbers to complete the calculation.");
      }
   }

}